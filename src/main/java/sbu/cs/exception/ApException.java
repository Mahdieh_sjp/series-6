package sbu.cs.exception;

public class ApException extends RuntimeException {

    private String message;

    public ApException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

}
