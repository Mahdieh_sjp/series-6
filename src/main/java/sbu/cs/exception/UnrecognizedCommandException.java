package sbu.cs.exception;

public class UnrecognizedCommandException extends ApException{
    public UnrecognizedCommandException() {
        super("%s in not a recognizable command");
    }
}
