package sbu.cs.exception;

public class NotImplementedCommandException extends ApException{
    public NotImplementedCommandException() {
        super("%s is not an implemented command!");
    }
}
