package sbu.cs.multithread.semaphor;

import java.util.concurrent.CountDownLatch;

public class SourceControllerPriority {

    public static void main(String[] args) throws InterruptedException {
        Source.setSem(2);
        CountDownLatch latch = new CountDownLatch(5);

        Chef chef1 = new Chef("chef1", latch);
        Chef chef2 = new Chef("chef2", latch);
        Chef chef3 = new Chef("chef3", latch);
        Chef chef4 = new Chef("chef4", latch);
        Chef chef5 = new Chef("chef5", latch);

        chef1.start();
        Thread.sleep(100);
        chef2.start();
        Thread.sleep(100);
        chef3.start();
        Thread.sleep(100);
        chef4.start();
        Thread.sleep(100);
        chef5.start();
        latch.await();
    }
}
