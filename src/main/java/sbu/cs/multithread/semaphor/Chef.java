package sbu.cs.multithread.semaphor;

import java.util.concurrent.CountDownLatch;

public class Chef extends Thread {
    CountDownLatch latch;

    public Chef(String name, CountDownLatch latch) {
        super(name);
        this.latch = latch;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {

            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            latch.countDown();
            Source.getSource(i, getName(), currentThread().getPriority());

        }
    }
}
