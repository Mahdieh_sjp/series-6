package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Source {

    static Semaphore sem;

    static void setSem(int permits){
        sem = new Semaphore(permits);
    }

    public static void getSource(int operation, String chefName, int priority) {

        try {
            System.out.println(chefName + " arrived." );
            Thread.sleep(500);
            sem.acquire();

            System.out.printf(chefName + " (priority : %d ) entered.\n", priority);

            Thread.sleep(500);

            System.out.printf("%d chef(s) is waiting for %s to leave.\n", sem.getQueueLength(), chefName);
            System.out.printf("Operation %d by %s ...\n", operation, chefName);

            Thread.sleep(2000);

            System.out.println(chefName + " left.");

            sem.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
