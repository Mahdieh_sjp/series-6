package sbu.cs.multithread.semaphor;


import java.util.concurrent.CountDownLatch;

public class SourceController {

    public static void main(String[] args) throws InterruptedException {
        Source.setSem(2);
        CountDownLatch latch = new CountDownLatch(5);

        Chef chef1 = new Chef("chef1", latch);
        Chef chef2 = new Chef("chef2", latch);
        Chef chef3 = new Chef("chef3", latch);
        Chef chef4 = new Chef("chef4", latch);
        Chef chef5 = new Chef("chef5", latch);
        chef1.setPriority(1);
        chef2.setPriority(1);
        chef3.setPriority(1);
        chef4.setPriority(1);
        chef5.setPriority(1);
        chef1.start();
        chef2.start();
        chef3.start();
        chef4.start();
        chef5.start();
        latch.await();
    }
}
