package sbu.cs.multithread.pi;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class PICalculator {

    private static final ExecutorService executor = Executors.newFixedThreadPool(8);

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */


    public String calculate(int floatingPoint) {

        try {
            for (int i = 0; i < 1000; i += Calculator.roundInEachThread) {
                executor.submit(new Calculator());
            }
            executor.shutdown();
            executor.awaitTermination(10000, TimeUnit.MILLISECONDS);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return PI.getPiValue(floatingPoint);
    }

}
