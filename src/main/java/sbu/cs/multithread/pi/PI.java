package sbu.cs.multithread.pi;

import java.math.BigDecimal;

import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicLong;

public class PI {

    private static BigDecimal piValue = new BigDecimal(0).setScale(1000, RoundingMode.HALF_DOWN);
    private static AtomicLong round =  new AtomicLong(0);


    synchronized public static void plus(BigDecimal bd) {
        piValue = piValue.add(bd);
    }

    synchronized public static long getAndIncrementRound(){
        return round.getAndIncrement();
    }

    public static String getPiValue(int floatingPoint){
        return piValue.toString().substring(0, floatingPoint+2);
    }

}
