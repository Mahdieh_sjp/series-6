package sbu.cs.multithread.pi;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator implements Runnable{

    static final int roundInEachThread = 1;

    @Override
    public void run() {
        for (int j = 0; j < roundInEachThread; j++){
            calculate();
        }
    }

    public void calculate(){
        double i = (double)PI.getAndIncrementRound();

        BigDecimal termOne = new BigDecimal(1).divide(BigDecimal.valueOf(16).pow((int) i), 1000, RoundingMode.HALF_EVEN);
        BigDecimal termTwo = new BigDecimal(4).divide(BigDecimal.valueOf(8*i+1), 1000, RoundingMode.HALF_EVEN);
        BigDecimal termThree = new BigDecimal(2).divide(BigDecimal.valueOf(8*i+4), 1000, RoundingMode.HALF_EVEN);
        BigDecimal termFour = new BigDecimal(1).divide(BigDecimal.valueOf(8*i+5), 1000, RoundingMode.HALF_EVEN);
        BigDecimal termFive = new BigDecimal(1).divide(BigDecimal.valueOf(8*i+6), 1000, RoundingMode.HALF_EVEN);

        PI.plus(termOne.multiply(termTwo.subtract(termThree)
                .subtract(termFour)
                .subtract(termFive)));
    }

}
